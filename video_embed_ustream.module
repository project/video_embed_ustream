<?php

/**
 * @file
 * Module for adding ustream videos to page.
 *
 * Add a handler for Ustream videos to Video Embed Field with this module you
 * can add videos from ustream.com.
 */

define('VIDEO_EMBED_USTREAM_MEDIA_USTREAM_REST_API', 'https://api.ustream.tv');

/**
 * Implements hook_help().
 */
function video_embed_ustream_help($path, $arg) {
  switch ($path) {
    case 'admin/help#video_embed_ustream':
      return check_markup(file_get_contents(dirname(__FILE__) . "/README.txt"));
  }
}

/**
 * Implements hook_video_embed_handler_info().
 */
function video_embed_ustream_video_embed_handler_info() {
  $handlers = array();
  $handlers['ustream'] = array(
    'title' => 'Ustream Video',
    'function' => 'video_embed_ustream_handle_video',
    'thumbnail_function' => 'video_embed_ustream_handle_thumbnail',
    'form' => 'video_embed_ustream_form',
    'domains' => array(
      'ustream.tv',
    ),
    'defaults' => array(
      'width' => 640,
      'height' => 480,
      'allowautoplay' => FALSE,
    ),
  );
  return $handlers;
}

/**
 * Form constructor for video embed ustream form.
 *
 * @param array $defaults
 *
 * @return array $form
 */
function video_embed_ustream_form(array $defaults) {
  $form = array();
  $form['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Player Height'),
    '#description' => t('The height of the player.'),
    '#default_value' => $defaults['height'],
    '#element_validate' => array('_video_embed_ustream_element_validate'),
  );
  $form['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Player Width'),
    '#description' => t('The width of the player.'),
    '#default_value' => $defaults['width'],
    '#element_validate' => array('_video_embed_ustream_element_validate'),
  );

  $form['allowautoplay'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow Autoplay'),
    '#desecription' => t('This will allow the video to autoplay.'),
    '#default_value' => $defaults['allowautoplay'],
  );
  return $form;
}

/**
 * This is the video handler (the 'function' key from handler_info).
 *
 * @param string $url
 *   The full video url.
 * @param array $settings
 *   An associative array of this handlers settings, from the settings form.
 *
 * @return string
 *   The embed code for the video.
 */
function video_embed_ustream_handle_video($url, $settings) {
  $width = $settings['width'];
  $height = $settings['height'];
  $autoplay = $settings['allowautoplay'];

  $normal_uri = _video_embed_ustream_parse_url($url);
  $url_type = _video_embed_ustream_check_type($url);
  $embed_url = _video_embed_ustream_get_oembedcode($normal_uri, $url_type, $autoplay);

  if ($embed_url) {
    $emb = "<iframe src=' " . $embed_url . " '    width=' " . $width . " '    height=' " . $height . " '  class='media-ustream-player' style='border: 0' ></iframe>";
    $video = array(
      '#markup' => $emb,
    );
    return $video;
  }
  return FALSE;
}

/**
 * Implements video_embed_ustream_check_type().
 */
function _video_embed_ustream_check_type($embed_code) {
  $embed_code = trim($embed_code);

  $recorded_patterns = array(
      // Matches the URL for the video page.
    '@ustream\.tv/recorded/(\d+)@i'             => 'recorded',
      // Matches the URL from the embed code for recorded videos.
    '@ustream\.tv/embed/recorded/([^"\&\?]+)@i' => 'recorded',
      // Channel.
    '@ustream\.tv/channel/([a-zA-Z0-9_\-]+)@i'  => 'live',
      // Channel embed.
    '@ustream\.tv/embed/([^"\&\?]+)@i'          => 'live',
      // Matches the URL for the user page.
    '@ustream\.tv/user/([a-zA-Z0-9_\-]+)@i'     => 'user',
      // User channel or promoted channel.
    '@ustream\.tv/([a-zA-Z0-9_\-]+)$@i'         => 'channeloruser',
  );

  foreach ($recorded_patterns as $pattern => $type) {
    preg_match($pattern, $embed_code, $matches);
    if (!empty($matches[1])) {
      return $type;
    }
  }
}

/**
 * Implements video_embed_ustream_parse_url().
 */
function _video_embed_ustream_parse_url($embed_code) {

  $embed_code = trim($embed_code);

  // Patterns.
  $recorded_patterns = array(
      // Matches the URL for the video page.
    '@ustream\.tv/recorded/(\d+)@i'             => 'recorded',
      // Matches the URL from the embed code for recorded videos.
    '@ustream\.tv/embed/recorded/([^"\&\?]+)@i' => 'recorded',
      // Channel.
    '@ustream\.tv/channel/([a-zA-Z0-9_\-]+)@i'  => 'live',
      // Channel embed.
    '@ustream\.tv/embed/([^"\&\?]+)@i'          => 'live',
      // Matches the URL for the user page.
    '@ustream\.tv/user/([a-zA-Z0-9_\-]+)@i'     => 'user',
      // User channel or promoted channel.
    '@ustream\.tv/([a-zA-Z0-9_\-]+)$@i'         => 'channeloruser',
  );

  foreach ($recorded_patterns as $pattern => $type) {
    preg_match($pattern, $embed_code, $matches);
    if (!empty($matches[1])) {
      try {

        if ($properties = _video_embed_ustream_valid_id(check_plain($matches[1]), $type)) {
          if (empty($properties['error'])) {
            $file_uri = $type . '/' . check_plain($properties['results']['id']);
            if ($type == 'channeloruser') {
              $file_uri = 'live/' . check_plain($properties['results']['id']);
            }
          }
        }
      }
      catch (Exception $e) {
        watchdog('widget', $e->getMessage(), WATCHDOG_ERROR);
      }

    }
  }

  if (isset($file_uri)) {
    $uri = file_stream_wrapper_uri_normalize('ustream://' . $file_uri);
    return $uri;
  }
}

/**
 * Implements video_embed_ustream_getVideoProperties().
 */
function _video_embed_ustream_get_video_properties($id, $refresh = FALSE) {
  $response = drupal_http_request(VIDEO_EMBED_USTREAM_MEDIA_USTREAM_REST_API . '/json/video/' . $id . '/getInfo');
  $data = drupal_json_decode($response->data);
  return $data;
}

/**
 * Implements video_embed_ustream_getChannelProperties().
 */
function _video_embed_ustream_get_channel_properties($id, $refresh = FALSE) {
  $response = drupal_http_request(VIDEO_EMBED_USTREAM_MEDIA_USTREAM_REST_API . '/json/channel/' . $id . '/getInfo');
  $data = drupal_json_decode($response->data);
  return $data;
}

/**
 * Implements video_embed_ustream_getUserProperties().
 */
function _video_embed_ustream_get_user_properties($id, $refresh = FALSE) {
  $response = drupal_http_request(VIDEO_EMBED_USTREAM_MEDIA_USTREAM_REST_API . '/json/user/' . $id . '/getInfo');
  $data = drupal_json_decode($response->data);
  return $data;
}

/**
 * Implements video_embed_ustream_valid_id().
 */
function _video_embed_ustream_valid_id($id, $type, $refresh = FALSE) {

  // Now lets validate per type.
  switch ($type) {
    case 'recorded':
        $properties = _video_embed_ustream_get_video_properties($id);
      if (!empty($properties['error'])) {
        throw new Exception(
            "The UStream video ID '$id' does not exist, is set to private, or has been deleted.");
      }
      break;

    case 'live':
        $properties = _video_embed_ustream_get_channel_properties($id);
      if (!empty($properties['error']) || $properties['results']['title'] == NULL) {
        throw new Exception(
            "The UStream channel ID '$id' does not exist, is set to private, or has been deleted.");
      }
      break;

    case 'user':
        $properties = _video_embed_ustream_get_user_properties($id);
      if (!empty($properties['error'])) {
        throw new Exception(
            "The UStream user ID '$id' does not exist, is set to private, or has been deleted.");
      }
      break;
  }

  // If we have a result, cache and return it, else return false.
  if (!empty($properties) && empty($properties['error'])) {
    return $properties;
  }
  return FALSE;
}

/**
 * Helper function to check if the value previously exists or not.
 */
function _video_embed_ustream_known_id($id, $type) {
  if ($type == 'channeloruser') {
    // TODO: PDO-dify this. (can EntityFieldQuery do this?).
    $result = db_query("SELECT fid FROM {file_managed} WHERE REPLACE(filename, ' ', '') = :id", array(':id' => $id));
    $data = $result->fetchAll();
    if (!empty($data[0])) {
      return $data[0]->fid;
    }
    return FALSE;
  }

  $file_query = new EntityFieldQuery();
  $result = $file_query->entityCondition('entity_type', 'file')
      ->propertyCondition('uri', "ustream://$type/$id")
      ->execute();
  if (!empty($result['file'])) {
    return key($result['file']);
  }
  return FALSE;
}

/**
 * Helper function for generating embed code for ustream.
 */
function _video_embed_ustream_get_oembedcode($uri, $url_type, $autoplay = 1) {

  $params = _video_embed_ustream_get_video_id($uri);
  if ($params) {
    $query = array(
      'wmode' => 'direct',
    );
    $query['autoplay'] = $autoplay;
    if ($url_type == 'recorded') {
      $url = "http://www.ustream.tv/embed/recorded/{$params['video_id']}";
    }
    else {
      $url = "http://www.ustream.tv/embed/{$params['video_id']}";
    }
    $embed_url = url($url, array('query' => $query, 'external' => TRUE));
    return $embed_url;
  }
  else {
    return FALSE;
  }

}

/**
 * Helper function for getting video id from ustream url.
 */
function _video_embed_ustream_get_video_id($url) {
  $path = explode('://', $url);
  if (count($path) == 2) {
    $parts = explode('/', $path[1]);
    $total = count($parts);
    if ($total != 2 || $path[0] != 'ustream') {
      return FALSE;
    }
    $params = array();
    $params['recorded'] = ($parts[0] == 'recorded');
    $params['video_id'] = $parts[1];
    if (count($parts) == 2) {
      return $params;
    }
    else {
      return FALSE;
    }
  }
  else {
    return FALSE;
  }

}

/**
 * Helper function to validate entered element value.
 */
function _video_embed_ustream_element_validate($element, &$form_state) {
  $check = _video_embed_ustream_check_input($element['#value']);
  if (!empty($element['#value']) && !$check) {
    form_error($element, t('The @name option must contain a valid value. You may either leave the text field empty or enter a string like "640px" or "640"', array('@name' => $element['#title'])));
  }
}

/**
 * Helper function to check if value is valid and not empty.
 *
 * @param string $input
 *   The string that contains input from setting form.
 *
 * @return bool
 *   If input is valid returns true else false
 */
function _video_embed_ustream_check_input($input) {
  $input = check_plain($input);
  if ($input == '') {
    return FALSE;
  }
  $size = drupal_strlen($input);
  $cur_char = '';
  for ($i = 0; $i < $size; $i++) {
    $cur_char = drupal_substr($input, $i, 1);
    if ($cur_char >= '0' && $cur_char <= '9') {
      continue;
    }
    break;
  }
  if ($i == $size) {
    return TRUE;
  }
  elseif ($i > 0 && $i == $size - 2) {
    if (drupal_substr($input, $i, 2) == 'px') {
      return TRUE;
    }
  }
  return FALSE;
}
